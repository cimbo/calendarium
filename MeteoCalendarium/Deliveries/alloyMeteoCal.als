
//SIGNATURES
sig Username{}
sig User{
name: one Username, cal: one Calendar
}
sig Calendar{
events: set Event }
abstract sig Event{ date: one Date,
startingHour: one Hour, endingHour: one Hour, place: one Place, creator: one User, invitations: set User
}

sig OutdoorEvent extends Event{ preference: some Preference
}
//Implemented for futher update
sig IndoorEvent extends Event{}
sig Preference extends WeatherCondition { }
abstract sig WeatherCondition{
mainCondition: one KindOfWeather, }
sig Forecast extends WeatherCondition{ place: one Place,
date: one Date, }
sig KindOfWeather{} abstract sig Notification{
owner: one User }
sig Invitation extends Notification{ ref: one Event
}

sig BadWeatherNotification extends Notification{ ref: one OutdoorEvent
}
sig Place{} sig Date{} sig Hour{
hour : one Int }
//FACT
//No calendar without user 
fact NoAloneCalendar{
no c: Calendar | all u: User|
c!=u.cal }
//hour strictly positive
fact HourPositive{ all h1:Hour|
h1.hour >0 }

//events created are in creator calendar
fact creatorCalendar{
all e1:Event | all user1 : User |
e1.creator = user1 implies e1 in user1.cal.events }
//A calendar different for all the user
fact oneCalendarOwner{
all u1:User | all u2:User | u1.cal=u2.cal iff u1=u2
}
//User can't invite himself
fact NoUserCanInviteHimSelf{ no i :Invitation |
i.owner=i.ref.creator }
//Creator not invited
fact creatorNotInvited{
all e:Event | e.creator not in e.invitations
}

//Informations associated at existing place
fact NoUselessForecastInformation { all f: Forecast |one p: Place | f.place=p
}
//Consistency of weather information according to the place and the time
fact NoForecastInfoForSamePlaceAndTimeDifferent{
all f1: Forecast | no f2: Forecast | (f1.place=f2.place)and(f1.date=f2.date)and(f1.mainCondition!=f2.mainCondition)
}
//Preference associated at existing outdoor event
fact NoRandomPreference{
all p : Preference | some eo: OutdoorEvent |
eo.preference = p
}
//Forecast for date sets in at least an evet
fact ForecastOnlyForDayEvent{
all f:Forecast | some e:Event| e.date=f.date and e.place=f.place
}

//Different starting hour from ending hour
fact DifferentHour{ all e:Event|
e.startingHour.hour < e.endingHour.hour }
//Same forecast for the same place and date
fact ForecastOne{
all f1:Forecast | all f2:Forecast |
(f1.date=f2.date and f1.place=f2.place) implies f1=f2
}
//No User With The Same User Name
fact NoDuplicatedUser{
all u1:User | all u2:User |
u1.name=u2.name implies u1=u2 }
//Constraint for the association between BadweatherNotifications and Events
fact ConsistenceWeatherNotification{
all b : BadWeatherNotification | all e: Event |
b.ref=e iff (some mainC : b.ref.preference.mainCondition | some f : Forecast | f.date=e.date and f.place=e.place and f.mainCondition = mainC)
}

//Bad weather notification only if there is a forecast for the event associated to the notification's event
fact BadWeatherNotificantionImpliesForecast{ all e : OutdoorEvent |
(some bad:BadWeatherNotification |bad.ref=e) implies (some f : Forecast | e.date=e.date and e.place=f.place)
}
//Only one bad Notification for outdoor event
fact OnlyOneBadNotificationForEventOutdoor{
all b1:BadWeatherNotification | all b2:BadWeatherNotification | b1.ref=b2.ref implies b1=b2
}
//Username exists only if exists one user owner
fact UsernameOnlyWithUser{
all u:Username | some user:User |
user.name=u
}
//Date with Event
fact UsefulDate{
all d: Date | some e: Event |
e.date=d }

//Useful place
fact UsefulPlace{
all p: Place | some e: Event |
e.place=p }
//Useful hour
fact UsefulHour{
all h: Hour | some e: Event |
e.startingHour = h || e.endingHour = h
}
//Useful weather
fact UsefulKindWeather{
all k: KindOfWeather | some f: Forecast|some p : Preference | f.mainCondition = k || p.mainCondition = k
}

//One event is in a Calendar iff the event's creator is the owner of the calendar or exists an invitation for that event
fact EventCorrectlyInACalendar{
all c : Calendar | all e : c.events | some u : User |
(u.cal=c) and ( ( e.creator = u ) or ( some i : Invitation | i.owner
=u and i.ref=e))
}
//Avoiding that an event starts on a certain hour and finishes before this hour 
fact HourCorrect{
all e: Event | e.startingHour.hour<=e.endingHour.hour }
//Avoiding overlap
fact NoOverlap{
all c: Calendar | all e1: c.events | all e2: c.events |
(e1.date=e2.date && e1!=e2 )implies (e1.startingHour.hour >= e2.endingHour.hour || e2.startingHour.hour >= e1.endingHour.hour)
}
fact AllPreference{
all oe: OutdoorEvent | all p1: oe.preference | all p2: oe.preference |
p1.mainCondition = p2.mainCondition implies p1=p2 }


//Now, we specify a predicate to show the global situation:
pred showGlobal{
#User = 2
#OutdoorEvent=2
# Forecast= 2
#BadWeatherNotification =1 #IndoorEvent=1 #Invitation=1
}
run showGlobal
